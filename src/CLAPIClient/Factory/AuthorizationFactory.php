<?php namespace CLAPIClient\Factory;

abstract class AuthorizationFactory {

    public static function create($path, $type) {

        switch (strtolower($type)) {
            case 'basic':
                return new \CLAPIClient\Additional\Headers($path);
                break;

            default:
                throw new \Exception("Authorization not supported", 1);
                break;
        }

    }

}