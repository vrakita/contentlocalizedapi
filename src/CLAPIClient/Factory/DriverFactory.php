<?php namespace CLAPIClient\Factory;

abstract class DriverFactory {


	public static function create($driver) {

		switch (strtolower($driver)) {
			case 'curl':
				return new \CLAPIClient\Drivers\CurlDriver;
				break;
			
			default:
				throw new \Exception("Driver not supported", 1);
				break;
		}

	}

}