<?php namespace CLAPIClient\Clients\DVIP;

use CLAPIClient\Contracts\MatchAdapter;

class LanguageMatch implements MatchAdapter {

    /**
     * Language code match array
     *
     * @var array
     */
    protected $mapper = [
        'da'    => 'dk',
        'ar'    => 'arabic',
        'sr'    => 'rs',
        'cs'    => 'cz',
        'sr_hr' => 'rs_hr',
        'el'    => 'gr',
        'ro_ro' => 'ro',
        'pt_br' => 'br',
        'sv'    => 'se'
    ];

    /**
     * Search through language array and return if found match if not return passed string
     *
     * @param string $code
     * @return string
     */
    public function match($code) {

        $code = strtolower($code);

        if( ! isset($this->mapper) || ! count($this->mapper)) return $code;

        return isset($this->mapper[$code]) ? $this->mapper[$code] : $code;

    }
}