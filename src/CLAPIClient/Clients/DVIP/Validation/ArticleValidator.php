<?php namespace CLAPIClient\Clients\DVIP\Validation;

class ArticleValidator {


    public static function validate($data) {

        if( ! isset($data['order']['language']) || $data['order']['language'] == '')    throw new \Exception('Source language is missing. Request not sent');
        if( ! isset($data['order']['content']) || $data['order']['content'] == '')      throw new \Exception('Target string is missing. Request not sent');
        if( ! isset($data['order']['title']) || $data['order']['title'] == '')          throw new \Exception('Target title is missing. Request not sent');
        if( ! isset($data['order']['writer']) || $data['order']['writer'] == '')        throw new \Exception('Writer\'s email is missing. Request not sent');
        if( ! isset($data['order']['domain']) || $data['order']['domain'] == '')        throw new \Exception('Writer\'s email is missing. Request not sent');

    }

}