<?php namespace CLAPIClient\Clients\DVIP\Validation;

class Validator {


    public static function validate($data) {

        if( ! isset($data['order']['from-language']) || $data['order']['from-language'] == '') throw new \Exception('Source language is missing. Request not sent');
        if( ! isset($data['order']['content']) || $data['order']['content'] == '') throw new \Exception('Target string is missing. Request not sent');
        if( empty($data['order']['translations']) || empty($data['order']['translations'][0])) throw new \Exception('Translation data is missing. Request not sent');

        $translation = $data['order']['translations'][0];

        if( ! isset($translation['to-language'])) throw new \Exception ('Target language is missing. Request not sent');
        if( ! isset($translation['writer'])) throw new \Exception ('Writer\'s email is missing. Request not sent');
        if( ! isset($translation['content'])) throw new \Exception ('Translation of string is missing. Request not sent');

    }

}