<?php namespace CLAPIClient\Clients\DVIP;

use CLAPIClient\Clients\DVIP\Validation\Validator;
use CLAPIClient\Contracts\MatchAdapter;
use CLAPIClient\Factory\AuthorizationFactory;
use CLAPIClient\Factory\DriverFactory;
use CLAPIClient\Files\ErrorHandler;
use CLAPIClient\Contracts\Sync;

class CompletedTranslation implements Sync {


    /**
     * Path to directory for logging errors
     *
     * @var bool
     */
    protected $logPath = false;

    /**
     * Sandbox URL
     *
     * @var string
     */
    protected $testURL = 'https://dev.contentlocalized.com/api/translation/completed';

    /**
     * Live URL
     *
     * @var string
     */
    protected $liveURL = 'https://contentlocalized.com/api/translation/completed';

    /**
     * Sandbox flag
     *
     * @var bool
     */
    protected $sandBox = false;

    /**
     * Driver for making request
     *
     * @var
     */
    protected $driver;

    /**
     * Data array which will be sent with request
     *
     * @var array
     */
    protected $data;

    /**
     * Source language of requested string
     *
     * @var string
     */
    protected $sourceLang = 'gb';

    /**
     * Target language of requested string
     * @var
     */
    protected $targetLang;

    /**
     * String which is translated
     *
     * @var
     */
    protected $content;

    /**
     * Email of writer
     *
     * @var
     */
    protected $writer;

    /**
     * Custom language mapper
     *
     * @var
     */
    protected $mapper;

    /**
     * Custom writer mapper
     *
     * @var
     */
    protected $writerMapper;

    /**
     * Response after request is completed
     *
     * @var
     */
    protected $response;

    public function __construct($path = false, $driver = 'curl') {

        $authHeaders = AuthorizationFactory::create($path, 'basic');

        $this->driver = DriverFactory::create($driver);

        $this->driver->setHeaders($authHeaders->makeAuthorizationHeaders())
            ->setHeaders('Content-type: application/json')
            ->post();

        $authHeaders->getSSL() ?: $this->driver->sslOff();

    }

    /**
     * Set writers email for completed work
     *
     * @param string $email
     * @return $this
     */
    public function setWriter($email) {
        $this->writer = $email;

        return $this;
    }

    /**
     * Set source language of translated string, default value is set to gb
     *
     * @param string $sourceLang
     * @return $this
     */
    public function setSourceLanguage($sourceLang) {
        $this->sourceLang = $sourceLang;

        return $this;
    }

    /**
     * Set target language of translated string
     *
     * @param $language
     * @return $this
     */
    public function setTranslationLanguage($language) {
        $this->targetLang = $language;

        return $this;
    }

    /**
     * Set sandbox mode
     *
     * @param bool|true $sandbox
     * @return $this
     */
    public function sandbox($sandbox = true) {

        $this->sandBox = $sandbox;

        return $this;

    }

    /**
     * Set original string
     *
     * @param string $string
     * @return $this
     */
    public function setString($string) {
        $this->data['order']['content'] = $string;

        return $this;
    }

    /**
     * Set translation for string
     *
     * @param string $string
     * @return $this
     */
    public function setTranslatedString($string) {
        $this->content = $string;

        return $this;
    }

    protected function setData() {
        $this->data['order']['instructions'] 	= 'This is API sync call from DVIP Translations';
        $this->data['order']['from-language']   = isset($this->mapper) ? $this->mapper->match($this->sourceLang) : $this->sourceLang;
        $this->data['order']['translations'][]  = [
            'to-language' 	=> isset($this->mapper) ? $this->mapper->match($this->targetLang) : $this->targetLang,
            'writer'		=> isset($this->writerMapper) ? $this->writerMapper->match($this->writer) : $this->writer,
            'content'		=> $this->content
        ];

        Validator::validate($this->data);

        $this->driver->setData($this->data);

    }

    /**
     * Enable error logging
     *
     * @param bool|false $path
     * @return $this
     */
    public function log($path = false) {

        $this->logPath = $path;

        return $this;

    }

    /**
     * Set custom mapper
     *
     * @param MatchAdapter $mapper
     * @return $this
     */
    public function setMapper(MatchAdapter $mapper) {

        $this->mapper = $mapper;

        return $this;

    }

    /**
     * Set custom writer mapper
     *
     * @param MatchAdapter $mapper
     * @return $this
     */
    public function setWriterMapper(MatchAdapter $mapper) {

        $this->writerMapper = $mapper;

        return $this;

    }

    /**
     * Make sync call
     *
     * @return mixed
     * @throws \Exception
     */
    public function send() {

        $this->setData();

        $this->driver->setUrl($this->sandBox ? $this->testURL : $this->liveURL);

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) {

            if($this->logPath) (new ErrorHandler())->save($this->logPath, $this->driver->getResponseCode() . ': ' . $response . ' * ' . serialize($this->data));

            throw new \Exception('Server error (' . $this->driver->getResponseCode() . '): ' . $response);

        }

        return $response;

    }

}