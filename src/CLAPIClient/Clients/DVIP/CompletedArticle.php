<?php namespace CLAPIClient\Clients\DVIP;

use CLAPIClient\Clients\DVIP\Validation\ArticleValidator;
use CLAPIClient\Contracts\ArticleSync;
use CLAPIClient\Contracts\MatchAdapter;
use CLAPIClient\Factory\AuthorizationFactory;
use CLAPIClient\Factory\DriverFactory;
use CLAPIClient\Files\ErrorHandler;

class CompletedArticle implements ArticleSync {


    /**
     * Path to directory for logging errors
     *
     * @var bool
     */
    protected $logPath = false;

    /**
     * Sandbox URL
     *
     * @var string
     */
    protected $testURL = 'https://dev.contentlocalized.com/api/article/completed';

    /**
     * Live URL
     *
     * @var string
     */
    protected $liveURL = 'https://contentlocalized.com/api/article/completed';

    /**
     * Sandbox flag
     *
     * @var bool
     */
    protected $sandBox = false;

    /**
     * Driver for making request
     *
     * @var
     */
    protected $driver;

    /**
     * Data array which will be sent with request
     *
     * @var array
     */
    protected $data;

    /**
     * Target language of requested string
     * @var
     */
    protected $targetLang;

    /**
     * Email of writer
     *
     * @var
     */
    protected $writer;

    /**
     * Custom language mapper
     *
     * @var
     */
    protected $mapper;

    /**
     * Custom writer mapper
     *
     * @var
     */
    protected $writerMapper;

    /**
     * Response after request is completed
     *
     * @var
     */
    protected $response;

    public function __construct($path = false, $driver = 'curl') {

        $authHeaders = AuthorizationFactory::create($path, 'basic');

        $this->driver = DriverFactory::create($driver);

        $this->driver->setHeaders($authHeaders->makeAuthorizationHeaders())
            ->setHeaders('Content-type: application/json')
            ->post();

        $authHeaders->getSSL() ?: $this->driver->sslOff();

    }

    /**
     * Set writers email for completed work
     *
     * @param string $email
     * @return $this
     */
    public function setWriter($email) {
        $this->writer = $email;

        return $this;
    }

    /**
     * Set target language of translated string
     *
     * @param $language
     * @return $this
     */
    public function setTranslationLanguage($language) {
        $this->targetLang = $language;

        return $this;
    }

    /**
     * Set target title
     *
     * @param $title
     * @return $this
     */
    public function setTitle($title) {
        $this->data['order']['title'] = $title;

        return $this;
    }

    /**
     * Set target domain
     *
     * @param $domain
     * @return $this
     */
    public function setDomain($domain) {
        $this->data['order']['domain'] = $domain;

        return $this;
    }

    /**
     * Set sandbox mode
     *
     * @param bool|true $sandbox
     * @return $this
     */
    public function sandbox($sandbox = true) {

        $this->sandBox = $sandbox;

        return $this;

    }

    /**
     * Set original string
     *
     * @param string $string
     * @return $this
     */
    public function setString($string) {
        $this->data['order']['content'] = $string;

        return $this;
    }

    protected function setData() {
        $this->data['order']['instructions'] 	= 'This is API sync call from DVIP Articles';
        $this->data['order']['language']        = isset($this->mapper) ? $this->mapper->match($this->targetLang) : $this->targetLang;
        $this->data['order']['writer']          = $this->writer;

        ArticleValidator::validate($this->data);

        $this->driver->setData($this->data);

    }

    /**
     * Enable error logging
     *
     * @param bool|false $path
     * @return $this
     */
    public function log($path = false) {

        $this->logPath = $path;

        return $this;

    }

    /**
     * Set custom mapper
     *
     * @param MatchAdapter $mapper
     * @return $this
     */
    public function setMapper(MatchAdapter $mapper) {

        $this->mapper = $mapper;

        return $this;

    }

    /**
     * Set custom writer mapper
     *
     * @param MatchAdapter $mapper
     * @return $this
     */
    public function setWriterMapper(MatchAdapter $mapper) {

        $this->writerMapper = $mapper;

        return $this;

    }

    /**
     * Make sync call
     *
     * @return mixed
     * @throws \Exception
     */
    public function send() {

        $this->setData();

        $this->driver->setUrl($this->sandBox ? $this->testURL : $this->liveURL);

        $response = $this->driver->call();

        if($this->driver->getResponseCode() !== 200) {

            if($this->logPath) (new ErrorHandler())->save($this->logPath, $this->driver->getResponseCode() . ': ' . $response . ' * ' . serialize($this->data));

            throw new \Exception('Server error (' . $this->driver->getResponseCode() . '): ' . $response);

        }

        return $response;

    }

}