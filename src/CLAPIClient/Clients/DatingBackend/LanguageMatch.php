<?php namespace CLAPIClient\Clients\DatingBackend;

use CLAPIClient\Contracts\MatchAdapter;


class LanguageMatch implements MatchAdapter {

    /**
     * Language code match array
     *
     * @var array
     */
    protected $mapper = [
        'cs' => 'cz',
        'da' => 'dk',
        'el' => 'gr',
        'sr' => 'rs',
        'sv' => 'se'
    ];

    /**
     * Search through language array and return if found match if not return passed string
     *
     * @param string $code
     * @return string
     */
    public function match($code) {

        if( ! isset($this->mapper) || ! count($this->mapper)) return $code;

        return isset($this->mapper[$code]) ? $this->mapper[$code] : $code;

    }

}