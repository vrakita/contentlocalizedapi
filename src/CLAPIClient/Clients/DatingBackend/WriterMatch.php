<?php namespace CLAPIClient\Clients\DatingBackend;

use CLAPIClient\Contracts\MatchAdapter;

class WriterMatch implements MatchAdapter {

    /**
     * Search through writer array and return if found match if not return passed string
     *
     * @param string $code
     * @return string
     */
    public function match($code) {

        if( ! isset($this->mapper) || ! count($this->mapper)) return $code;

        return isset($this->mapper[$code]) ? $this->mapper[$code] : $code;

    }

    protected $mapper = [

        'filipedelman@gmail.com'        => 'filip@firstbeatmedia.com',
        'melanie.kuehs@gmail.com'       => 'melanie@firstbeatmedia.com',
        'ezequielmeilij@gmail.com'      => 'izzy@firstbeatmedia.com',
        'fabioizzo77@gmail.com'         => 'fabioizzo@firstbeatmedia.com',
        'martynaalvarado@gmail.com'     => 'martyna@firstbeatmedia.com',
        'zivkovicbane@gmail.com'        => 'bane@firstbeatmedia.com',
        'monamang@gmail.com'            => 'mmangat@firstbeatmedia.com',
        'enthusiastlunaa@gmail.com'     => 'luna@firstbeatmedia.com',
        'nena.cicmil@gmail.com'         => 'nenacicmil@firstbeatmedia.com',
        'naca0475@gmail.com'            => 'natacha@firstbeatmedia.com',
        'julijasuslova@gmail.com'       => 'julija@firstbeatmedia.com',
        'evren.altinkas@gmail.com'      => 'evren@firstbeatmedia.com',
        'teija.devere@gmail.com'        => 'teija@firstbeatmedia.com',
        'jovanovic.nnd@gmail.com'       => 'njovanovic@firstbeatmedia.com',
        'serbiangie@gmail.com'          => 'anap@firstbeatmedia.com',
        'djula.gabor@gmail.com'         => 'djula@firstbeatmedia.com',
        'mgfriasr@gmail.com'            => 'marigaby@firstbeatmedia.com',
        'ramonmartensen@gmail.com'      => 'ramonmartensen@firstbeatmedia.com'

    ];
}