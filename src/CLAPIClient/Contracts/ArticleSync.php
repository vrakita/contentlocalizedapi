<?php namespace CLAPIClient\Contracts;

interface ArticleSync {

    /**
     * Set writers email for completed work
     *
     * @param string $email
     * @return $this
     */
    public function setWriter($email);

    /**
     * Set target language of translated string
     *
     * @param $language
     * @return $this
     */
    public function setTranslationLanguage($language);

    /**
     * Set original string
     *
     * @param string $string
     * @return $this
     */
    public function setString($string);

    /**
     * Set target title
     *
     * @param $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Set target domain
     *
     * @param $domain
     * @return $this
     */
    public function setDomain($domain);

    /**
     * Set custom mapper
     *
     * @param MatchAdapter $mapper
     * @return $this
     */
    public function setMapper(MatchAdapter $mapper);

    /**
     * Set custom writer mapper
     *
     * @param MatchAdapter $mapper
     * @return $this
     */
    public function setWriterMapper(MatchAdapter $mapper);

    /**
     * Make sync call
     *
     * @return mixed
     * @throws \Exception
     */
    public function send();

}