<?php namespace CLAPIClient\Contracts;

interface FileManager {

    public function save($path, $content);

}