<?php namespace CLAPIClient\Contracts;

interface MatchAdapter {

    /**
     * Search through language array and return if found match if not return passed string
     *
     * @param string $code
     * @return string
     */
    public function match($code);

}