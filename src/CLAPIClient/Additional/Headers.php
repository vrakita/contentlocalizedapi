<?php namespace CLAPIClient\Additional;

use CLAPIClient\Contracts\AuthorizationHeaders;
use Dotenv\Dotenv;

class Headers implements AuthorizationHeaders {

    public function __construct($path) {

        if($path) {

            $loader = new Dotenv($path);
            $loader->load();
            $loader->required(['CL_EMAIL', 'CL_PASSWORD', 'CL_SSL']);

        }

        if( ! getenv('CL_EMAIL') || ! getenv('CL_PASSWORD'))
            throw new \Exception('Platform credentials missing.');


    }

    public function makeAuthorizationHeaders() {

        return 'Authorization: Basic '. base64_encode(getenv('CL_EMAIL') . ':' . getenv('CL_PASSWORD'));

    }

    public function getSSL() {

        return getenv('CL_SSL');

    }

}