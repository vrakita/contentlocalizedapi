<?php

require 'vendor/autoload.php';

use CLAPIClient\Clients\DVIP\CompletedTranslation;
use CLAPIClient\Clients\DVIP\LanguageMatch;

try {

    // If getenv('CL_EMAIL') and getenv('CL_PASSWORD') are available as environment variables,
    // then we do not need path parameter to the .env file in object constructor !!!
    $sync = new CompletedTranslation(__DIR__);

    $response = $sync
        // Set author of string changes
        ->setWriter('dawn@firstbeatmedia.com')
        // Set string which is translating
        ->setString('new string 2')
        // Set translation of string
        ->setTranslatedString('hello world uk')
        // Set language of translation
        ->setTranslationLanguage('rs')
        // Log errors to directory (optional)
        ->log(__DIR__)
        // Set custom mapper for different language code interpretations between platforms
        ->setMapper(new LanguageMatch())
        // Send request
        ->send();

} catch (\Exception $e) {

    echo $e->getMessage();
    exit;

}

var_dump($response);