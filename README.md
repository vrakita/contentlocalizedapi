# Content Localized API #

### Installation: ###

Add this to your composer.json

```json
{
    "require": {
        "vrakita/contentlocalizedapi": "1.*"
    },
    "config": {
        "preferred-install": "dist"
    }
}
```

Update your dependencies
```sh
composer update
```

Create .env file with your ContentLocalized credentials
```sh
CL_EMAIL=mymail@firstbeatmedia.com
CL_PASSWORD=secretpassword
CL_SSL=1
```

### Example: ###


```php
<?php

require 'vendor/autoload.php';

use CLAPIClient\Clients\DVIP\CompletedTranslation;

try {

    $sync = new CompletedTranslation(__DIR__);

    $response = $sync
        // Set author of string changes
        ->setWriter('writer@test.ff')
        // Set string which is translating
        ->setString('new string 2')
        // Set translation of string
        ->setTranslatedString('hello world uk')
        // Set language of translation
        ->setTranslationLanguage('rs')
        // Sandbox mode http://dev.contentlocalized.com/
        ->sandbox()
        // Log errors to directory
        ->log(__DIR__)
        // Send request
        ->send();

} catch (\Exception $e) {

    echo $e->getMessage();
    exit;

}



var_dump($response);
```